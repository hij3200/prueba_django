# Mi primer proyecto Django

El objetivo de este proyecto es aprender a utilizar django 2.2 en una maquina virtual gestionada por Vagrant.

A continuación se describe el proceso de configuración inicial del proyecto. 

Tome en cuenta que lo ideal es compartir la configuración de vagrant con herramientas que este mismo software provee.

## Herramientas de desarrollo

- Windows 10 Pro
- [Vagrant 2.2.7](https://www.vagrantup.com/downloads.html)
- [ubuntu/xenial64 Vagrant box](https://app.vagrantup.com/ubuntu/boxes/xenial64) v20200407.0.0
- [Django 2.2](https://docs.djangoproject.com/en/3.0/releases/2.2/)
- [Python 3.6](https://www.python.org/downloads/release/python-360/)
- [PostgreSQL](https://www.postgresql.org/download/linux/ubuntu/) 11.7
- [Virtualenv](https://virtualenv.pypa.io/en/latest/)


## Configuración inicial Vagrant

Para crear e iniciar ububtu/xenial64 vagrant box: 
- Crear la carpeta que contendrá la máquina virtual
- Ejecutar los siguientes comandos en el cmd de windows

```
#crear
vagrant box add ubuntu/xenial64
vagrant init ubuntu/xenial

#ejecutar
vagrant up

#acceder
vagrant ssh
```
Para detener la máquina virtual, abrir un nuevo cmd y, en la misma carpeta, ejecutar `vagrant suspend`

## Instalación de python 3.6

Luego de ingresar a la máquina virtual, se debe cambiar la version de python (por defecto 3.5) con los siguientes comandos: 

```bash
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update
sudo apt install python3.6 python3.6-dev python-dev
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.5 1
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 2

#seleccionar 0 al ejecutar:
sudo update-alternatives --config python3

#verificar la version de python
python3 -V
```

## Configurar Virtualenv

Luego de ingresar a la máquina virtual, se debe cambiar la version de python (por defecto 3.5) con los siguientes comandos: 

```bash
##instalar y configurar
sudo apt install python3-pip
sudo apt install virtualenv

#en la carpeta de los proyectos, crear un nuevo entorno virtual 
#especificando la version de python a usar
virtualenv --python=/usr/bin/python3.6 env

#ejecutar entorno
source env/bin/activate

#dentro del entorno, comprobar version de python 3.6
python --version

#para salir del entorno 
deactivate
```

## Antes de crear proyectos en Django

Para acceder a los archivos del proyecto, se debe sincronizar la carpeta de proyectos entre la virtual y la máquina host. Además de configurar el puerto de salida para acceder a la ejecucion del proyecto.

- En la maquina HOST, en el directorio donde se encuentra la vagrant box, editar el vagrantfile:
```bash
start notepad vagrantfile
```
- Dentro del archivo:
```
#Descomentar y cambiar la linea:
config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"
#por:
config.vm.network "forwarded_port", guest: [puerto_django], host: [puerto_deseado], host_ip: "127.0.0.1"
```
- En la parte del documento que dice 'Share an additional folder to the guest VM. The first argument..' copiar la ultima línea de ese bloque y agregarla en la siguiente:
```
#copiar y cambiar
config.vm.synced_folder "../data", "/vagrant_data"
#por 
config.vm.synced_folder "carpetaHost", "/home/usuario/carpetaCompartir", owner: "usuario_vm", group: "usuario_vm", mount_options: ["dmode=775,fmode=664"]
```

- Guardar archivo vagrantfile
- Reiniciar la maquina con `vagrant reload`

## Crear proyecto

Acceder nuevamente a la máquina virtual con `vagrant ssh` y ejecutar los siguientes comandos:


```bash
#activar entorno
source env/bin/activate

#actualizar pip
pip install --upgrade pip

#instalar django
pip install Django==2.2

#puede clonar un poryecto en django en su carpeta de proyectos
#o crear el NUEVO proyecto con
django-admin.py startproject nombre_proyecto

#probar Ejecucion de proyecto
python manage.py runserver [0:puertoOpcional configurando en el vagrantfile]
```
Verificar en la máquina host que se puedea acceder a través del `http:\\localhost:[puerto]`

## Instalar PostgreSQL 11
Ejecutar los siguientes comandos (en la máquina virtual):
```bash
#Crear un nuevo archivo
sudo nano /etc/apt/sources.list.d/pgdg.list

#Agregar en el archivo la linea
deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main

#Importar la llave del repositorio y actualizar la lista de paquetes
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update

#instalar
sudo apt-get install postgresql-11

#Iniciar la Base de datos
pg_ctlcluster 11 main start

#agregar usuario a postgresql
sudo -u postgres createuser nombreUsuario -P
```
## Configurar base de datos en Django
- Ejecutar los siguientes comandos:
```bash
#crear la base de datos y establecer el usuario propietario
sudo -u postgres createdb nombreBase -O nombreUsuario

#OP1: primero instalar:
sudo apt-get install postgresql-server-dev-11 libpq-dev
pip install psycopg2 (sin sudo)

#OP2: sino funciona, tambien instalar las siguientes dependencias
sudo apt-get install python3.6-gdbm build-essential libssl-dev libffi-dev libxml2-dev libxslt1-dev zlib1g-dev
sudo apt-get install postgresql-server-dev-11 libpq-dev
pip install psycopg2 (sin sudo)

#configurar
nano carptetaProyecto/settings.py
```
- Dentro del archivo, comentar la configuracion por defecto de la BD y agregar:
```
DATABASES = {'default':
                 {'ENGINE': 'django.db.backends.postgresql_psycopg2',
                  'NAME': 'nombreBaseD',
                  'USER': 'usuario',
                  'PASSWORD': 'contrasena',
                  'HOST': 'localhost',
                  'PORT': '', }}
```
- Guardar archivo y ejecutar:
```bash
#Guardar y borrar la base por defecto
rm db.sqlite3

#ejecutar las migraciones
python manage.py migrate
```
- Para comprobar la existencia de las nuevas tablas
```bash
#Entrar en la bd
sudo -u postgres psql nombreBaseDatos

#Ver las tablas
\d
#salir de psql
\q
```




*Irene Delgado - FIA - Universidad de El Salvador - 2020*
